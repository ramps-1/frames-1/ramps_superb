

#
#	how to publish:
#
#		python3 structures/modules/ramps_majestic/_status/status.proc.py
#		cp structures/modules/ramps_majestic/module.MD readme.md
#		(rm -rf dist && python3 -m build && twine upload dist/*)
#		(rm -rf dist && rm -rf ramps_galactic.egg-info)
#


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])

def relative_path (path):
	import pathlib
	from os.path import dirname, join, normpath
	import sys

	this_directory_path = pathlib.Path (__file__).parent.resolve ()	

	return str (normpath (join (this_directory_path, path)))
	

import os
status_proc_path = relative_path ("../structures/modules/ramps_majestic/_status/status.proc.py")
readme_from_path = relative_path ("../structures/modules/ramps_majestic/module.MD")
readme_to_path = relative_path ("../readme.md")


os.system (f"python3 { status_proc_path }")
os.system (f"cp '{ readme_from_path }' '{ readme_to_path }'")

