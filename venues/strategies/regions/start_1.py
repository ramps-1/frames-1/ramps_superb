


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages',
	'../../stages_pip'
])

from datetime import datetime, timedelta

import pandas
import rich	

import ramps_galactic
import ramps_galactic.victory_multiplier.purchase_treasure_at_inclines as purchase_treasure_at_inclines_VM	
import ramps_galactic.victory_multiplier.purchase_treasure_over_span as purchase_treasure_over_span_VM
import ramps_galactic.example_data.read as read_example_data
import ramps_galactic.clouds.Yahoo.VLOCH as Yahoo_VLOCH

def galactic_ramp ():
	end_date = datetime.today ()
	start_date = end_date - timedelta(days=100)
	
	trend_DF = Yahoo_VLOCH.retrieve ({
		"symbol": "OTLY",
		
		"span": [
			start_date,
			end_date
		],
		
		"quant": "1h"
	})


	enhanced_trend_DF = ramps_galactic.calc (
		trend_DF,
		period = 14,
		multiplier = 2
	)
	enhanced_list = enhanced_trend_DF.to_dict ('records')


	'''
		This calculates the multipliers
	'''
	victory_multiplier_if_riding = purchase_treasure_at_inclines_VM.calc (
		enhanced_trend_DF,
		include_last_change = False
	)

	#rich.print_json (data = victory_multiplier_if_riding ["relevant"])	
	# rich.print_json (data = victory_multiplier_if_riding)

	victory_multiplier_if_holding = purchase_treasure_over_span_VM.calc (enhanced_trend_DF)
	current_trend = victory_multiplier_if_riding ["current trend"]


	rich.print_json (data = {
		"ramps galactic": {
			"The multiplier if holding over the entire interval:": victory_multiplier_if_holding,
			"The multiplier if trading the ramp:": victory_multiplier_if_riding ["treasure purchase victory multiplier"],
			"current move:": current_trend
		}
	})


	
	ramps_galactic.chart_the_data (
		enhanced_trend_DF,
		victory_multiplier_if_riding
	)
		
		

	
	
	
galactic_ramp ()